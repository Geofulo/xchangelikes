﻿using System;
using System.Collections.Generic;
using UIKit;
using Xamarin.Auth;
using Xamarin.Forms;
using XchangeLikes.Helpers;
using XchangeLikes.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(FacebookDependency))]

namespace XchangeLikes.iOS
{
	public class FacebookDependency : IFacebook
	{
		void IFacebook.Login()
		{
			var window = UIApplication.SharedApplication.KeyWindow;
			var vc = window.RootViewController;

			var auth = new OAuth2Authenticator(
				clientId: "246878555693567",
				scope: "public_profile,email,user_friends,user_likes,user_posts,user_photos,user_videos,manage_pages,publish_pages,pages_show_list,publish_actions",
				authorizeUrl: new Uri("https://m.facebook.com/dialog/oauth/"),
				redirectUrl: new Uri("https://www.facebook.com/connect/login_success.html")
			);

			auth.Completed += (sender, eventArgs) =>
			{
				if (eventArgs.IsAuthenticated)
				{
					string accessToken = eventArgs.Account.Properties["access_token"];

					Console.WriteLine("AUTH COMPLETED: " + eventArgs.Account);
					Console.WriteLine("AUTH COMPLETED TOKEN: " + accessToken);

					//AccountStore.Create().Save(eventArgs.Account, "Facebook");
					Settings.AccessToken = accessToken;

					App.WSRest.GetMyId();

					App.WSRest.LoginWithFacebook(accessToken);

					//App.CurrentUser = App.WSRest.GetUser(idUser);

					Xamarin.Forms.Application.Current.MainPage = new NavigationPage(new MainPage());
				}
			};

			vc.PresentViewController(auth.GetUI(), true, null);
		}

	}
}

