﻿using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using XchangeLikes;
using XchangeLikes.iOS;

[assembly: ExportRenderer(typeof(SwitchCustom), typeof(SwitchRender))]

namespace XchangeLikes.iOS
{
	public class SwitchRender : SwitchRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Switch> e)
		{
			base.OnElementChanged(e);

			var obj = (SwitchCustom)this.Element;
			if (Element == null)
				return;

			Control.TintColor = UIColor.Cyan;
			//Control.BackgroundColor = UIColor.Cyan;
			Control.OnTintColor = UIColor.Red;

		}
	}
}

