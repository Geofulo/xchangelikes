﻿using System;
using Foundation;
using UIKit;

namespace XchangeLikes.iOS
{
	class ExtendedUIWebViewDelegate : IUIWebViewDelegate
	{
		WebViewRender webViewRenderer;

		public ExtendedUIWebViewDelegate(WebViewRender _webViewRenderer = null)
		{
			webViewRenderer = _webViewRenderer ?? new WebViewRender();
		}

		public IntPtr Handle
		{
			get;
		}

		public void Dispose()
		{
			
		}

		[Export("webViewDidFinishLoad:")]
		public void LoadingFinished(UIWebView webView)
		{
			var wv = webViewRenderer.Element as WebViewCustom;
			if (wv != null)
			{
				//await System.Threading.Tasks.Task.Delay(100); wait here till content is rendered
				wv.HeightRequest = (double)webView.ScrollView.ContentSize.Height;
			}
		}

	}
}