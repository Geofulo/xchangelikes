﻿using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using XchangeLikes;
using XchangeLikes.iOS;

[assembly: ExportRenderer(typeof(SegmentedControl), typeof(SegmentedControlRenderer))]

namespace XchangeLikes.iOS
{
	public class SegmentedControlRenderer : ViewRenderer<SegmentedControl, UISegmentedControl>
	{
		public SegmentedControlRenderer()
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<SegmentedControl> e)
		{
			base.OnElementChanged(e);

			if (Element == null)
				return;

			var segmentedControl = new UISegmentedControl();

			if (e.NewElement != null)
			{
				for (var i = 0; i < e.NewElement.Children.Count; i++)
				{
					segmentedControl.InsertSegment(e.NewElement.Children[i].Text, i, false);
				}

				segmentedControl.ValueChanged += (sender, eventArgs) =>
				{
					e.NewElement.SelectedValue = segmentedControl.TitleAt(segmentedControl.SelectedSegment);
				};

				segmentedControl.SelectedSegment = 0;
				//segmentedControl.TintColor = ColorResources.NavBarText.ToUIColor();

				segmentedControl.SetTitleTextAttributes(
					new UITextAttributes
					{
						//Font = UIFont.FromName(Fonts.Regular, 14)
					},
					UIControlState.Normal
				);

				SetNativeControl(segmentedControl);
			}
		}
	}
}

