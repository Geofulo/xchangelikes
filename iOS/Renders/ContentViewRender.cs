﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using XchangeLikes;
using XchangeLikes.iOS;

[assembly: ExportRenderer(typeof(ContentViewCustom), typeof(ContentViewRender))]

namespace XchangeLikes.iOS
{
	public class ContentViewRender : ViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.View> e)
		{
			base.OnElementChanged(e);

			this.AutosizesSubviews = true;
			this.AutoresizingMask = UIKit.UIViewAutoresizing.FlexibleMargins;
			this.ContentMode = UIKit.UIViewContentMode.ScaleAspectFill;
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			this.AutosizesSubviews = true;
			this.AutoresizingMask = UIKit.UIViewAutoresizing.FlexibleMargins;
			this.ContentMode = UIKit.UIViewContentMode.ScaleAspectFill;
		}
	}
}

