// Helpers/Settings.cs
using Refractored.Xam.Settings;
using Refractored.Xam.Settings.Abstractions;

namespace XchangeLikes.Helpers
{
  /// <summary>
  /// This is the Settings static class that can be used in your Core solution or in any
  /// of your client applications. All settings are laid out the same exact way with getters
  /// and setters. 
  /// </summary>
  public static class Settings
  {
    private static ISettings AppSettings
    {
      get
      {
        return CrossSettings.Current;
      }
    }

    #region Setting Constants

    private const string SettingsKey = "settings_key";
		private static readonly string SettingsDefault = string.Empty;

		private const string AccessTokenKey = "accesstoken_key";
		private static readonly string AccessTokenDefault = string.Empty;

		private const string IdUserKey = "iduser_key";
		private static readonly string IdUserDefault = string.Empty;

		private const string NameKey = "name_key";
		private static readonly string NameDefault = string.Empty;

		private const string LinkKey = "link_key";
		private static readonly string LinkDefault = string.Empty;

		private const string TelerikIdUserKey = "telerik_iduser_key";
		private static readonly string TelerikIdUserDefault = string.Empty;

		#endregion


		public static string GeneralSettings
		{
			get
			{
				return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(SettingsKey, value);
			}
		}

		public static string AccessToken
		{
			get { return AppSettings.GetValueOrDefault(AccessTokenKey, AccessTokenDefault); }
			set { AppSettings.AddOrUpdateValue(AccessTokenKey, value); }
		}

		public static string IdUser
		{
			get { return AppSettings.GetValueOrDefault(IdUserKey, IdUserDefault); }
			set { AppSettings.AddOrUpdateValue(IdUserKey, value); }
		}

		public static string Name
		{
			get { return AppSettings.GetValueOrDefault(IdUserKey, IdUserDefault); }
			set { AppSettings.AddOrUpdateValue(IdUserKey, value); }
		}

		public static string Link
		{
			get { return AppSettings.GetValueOrDefault(LinkKey, LinkDefault); }
			set { AppSettings.AddOrUpdateValue(LinkKey, value); }
		}

		public static string TelerikIdUser
		{
			get { return AppSettings.GetValueOrDefault(TelerikIdUserKey, TelerikIdUserDefault); }
			set { AppSettings.AddOrUpdateValue(TelerikIdUserKey, value); }
		}

  }
}