﻿using System;
using Xamarin.Forms;
using XchangeLikes.Helpers;
using XchangeLikes.Resources;

namespace XchangeLikes
{
	public class MainPage : TabbedPage
	{
		ToolbarItem AddCoinsItem;

		public MainPage()
		{
			Title = "XchangeLikes";

			setItems();
			setMessageCenter();

			Children.Add(new CoinsPage(false));
			Children.Add(new CoinsPage(true));
			//Children.Add(new GetFeaturesPage());
			Children.Add(new EarnCoinsPage());
			Children.Add(new MorePage());
		}

		void setItems()
		{
			ToolbarItems.Clear();

			AddCoinsItem = new ToolbarItem();

			if (App.CurrentUser.CustomProperties["Coins"] != null)
				AddCoinsItem.Text = App.CurrentUser.CustomProperties["Coins"].ToString() + " " + StringsRes.CoinsTitle;
			else
				AddCoinsItem.Text = "0 " + StringsRes.CoinsTitle;

			ToolbarItems.Add(AddCoinsItem);
		}
	
		void setMessageCenter()
		{
			MessagingCenter.Subscribe<CoinsPage>(this, "ReloadCoins", (obj) => {
				App.CurrentUser = App.WSRest.GetUser(new Guid(Settings.TelerikIdUser));
				setItems();
			});
		}
	}
}

