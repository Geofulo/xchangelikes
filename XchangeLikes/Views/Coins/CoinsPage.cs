﻿using System;
using Xamarin.Forms;
using XchangeLikes.Helpers;
using XchangeLikes.Resources;

namespace XchangeLikes
{
	public class CoinsPage : ContentPage
	{
		bool IsFeature;

		Button BackButton;
		Label HelpCoinsLabel;

		Label CountLikesLabel;
		Slider CountLikesSlider;
		Button OrderLikesButton;

		ContentView MainView;
		ProfileLikesView ProfileLikesView;
		PagesLikesView PagesLikesView;
		PostsPagesLikesView PostsPagesLikesView;
		FriendsLikesView FriendsLikesView;

		SegmentedControl Segmented;

		SegmentedControlOption ProfileOption;
		SegmentedControlOption PagesOption;
		SegmentedControlOption FriendsOption;

		SwitchCustom LikesSwitch;

		public CoinsPage(bool IsFeature)
		{
			this.IsFeature = IsFeature;

			if (IsFeature)
				Title = StringsRes.GetFeaturesTitle;
			else
				Title = StringsRes.CoinsTitle;

			createElements();
			createLayouts();
			setListeners();

			Content = new StackLayout
			{
				Padding = new Thickness(0, 10, 0, 0),
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children =
				{
					new StackLayout {
						Padding = new Thickness (10, 0),
						VerticalOptions = LayoutOptions.FillAndExpand,
						Children =
						{
							new StackLayout {
								Orientation = StackOrientation.Horizontal,
								Children = {
									BackButton,
									HelpCoinsLabel
								}
							},
							CountLikesLabel,
							CountLikesSlider,
							new ContentView {
								//IsClippedToBounds = true,
								Content = OrderLikesButton,
							},
							MainView,
							new ContentView {
								VerticalOptions = LayoutOptions.End,
								Content = Segmented
							},
							new StackLayout {
								Padding = new Thickness(0, 15),
								Orientation = StackOrientation.Horizontal,
								HorizontalOptions = LayoutOptions.FillAndExpand,
								VerticalOptions = LayoutOptions.End,
								Children = 
								{
									new Label {
										Text = StringsRes.LikesTitle,
										VerticalOptions = LayoutOptions.CenterAndExpand,
										HorizontalOptions = LayoutOptions.StartAndExpand,
									},
									LikesSwitch,
									new Label {
										Text = StringsRes.FollowersTitle,
										VerticalOptions = LayoutOptions.CenterAndExpand,
										HorizontalOptions = LayoutOptions.EndAndExpand,
									}
								}
							}
						}
					},
				}
			};
		}

		void createElements()
		{
			BackButton = new Button
			{
				Text = "Back",
				IsVisible = false,
				HorizontalOptions = LayoutOptions.StartAndExpand,
			};

			HelpCoinsLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				TextColor = Color.Gray,
				HorizontalOptions = LayoutOptions.EndAndExpand,
			};

			if (IsFeature)
				HelpCoinsLabel.Text = "1 LIKE = 1 USD";
			else
				HelpCoinsLabel.Text = "1 LIKE = 1 COIN";

			CountLikesLabel = new Label
			{
				Text = "14",
				FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};

			CountLikesSlider = new Slider
			{
				Minimum = 0,
				Maximum = 50,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			CountLikesSlider.Value = 14;

			OrderLikesButton = new Button
			{
				BackgroundColor = Color.Silver,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start
			};

			if (IsFeature)
				OrderLikesButton.Text = StringsRes.BuyLikes + " 14 Likes";
			else
				OrderLikesButton.Text = StringsRes.OrderLikes + " 14 Likes";

			Segmented = new SegmentedControl { 
				VerticalOptions = LayoutOptions.End,
			};

			ProfileOption = new SegmentedControlOption
			{
				Text = StringsRes.ProfileOptionTitle
			};
			PagesOption = new SegmentedControlOption
			{
				Text = StringsRes.PagesOptionTitle
			};
			FriendsOption = new SegmentedControlOption
			{
				Text = StringsRes.FriendsOptionTitle
			};

			Segmented.Children.Add(ProfileOption);
			Segmented.Children.Add(PagesOption);
			Segmented.Children.Add(FriendsOption);

			Segmented.SelectedValue = StringsRes.ProfileOptionTitle;

			LikesSwitch = new SwitchCustom();

			MainView = new ContentView
			{
				Padding = new Thickness(0, 10),
				VerticalOptions = LayoutOptions.FillAndExpand
			};
		}

		void createLayouts()
		{
			ProfileLikesView = new ProfileLikesView();
			PagesLikesView = new PagesLikesView();
			FriendsLikesView = new FriendsLikesView();

			MainView.Content = ProfileLikesView;
		}

		void setListeners()
		{
			CountLikesSlider.ValueChanged += (sender, e) => {
				
				var count = (int) e.NewValue;

				CountLikesLabel.Text = "" + count.ToString();

				if (LikesSwitch.IsToggled)
				{
					if (IsFeature)
						OrderLikesButton.Text = StringsRes.BuyLikes + " " + count + " Followers";
					else
						OrderLikesButton.Text = StringsRes.OrderLikes + " " + count + " Followers";					
				}
				else
				{
					if (IsFeature)
						OrderLikesButton.Text = StringsRes.BuyLikes + " " + count + " Likes";
					else
						OrderLikesButton.Text = StringsRes.OrderLikes + " " + count + " Likes";					
				}
			};

			Segmented.ValueChanged += (sender, e) =>
			{
				System.Diagnostics.Debug.WriteLine(Segmented.SelectedValue);

				BackButton.IsVisible = false;
				OrderLikesButton.IsEnabled = true;

				if (Segmented.SelectedValue == StringsRes.ProfileOptionTitle)
				{
					MainView.Content = ProfileLikesView;
				}
				if (Segmented.SelectedValue == StringsRes.PagesOptionTitle)
				{
					MainView.Content = PagesLikesView;
					OrderLikesButton.IsEnabled = false;
				}
				if (Segmented.SelectedValue == StringsRes.FriendsOptionTitle)
				{
					MainView.Content = FriendsLikesView;
					OrderLikesButton.IsEnabled = false;
				}
			};

			PagesLikesView.PagesListView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null)
					return;
				
				var pageSelected = (PageModel)e.SelectedItem;

				PostsPagesLikesView = new PostsPagesLikesView(pageSelected);
				MainView.Content = PostsPagesLikesView;

				BackButton.IsVisible = true;
				OrderLikesButton.IsEnabled = true;

				PagesLikesView.PagesListView.SelectedItem = null;
			};

			BackButton.Clicked += (sender, e) => {
				BackButton.IsVisible = false;

				MainView.Content = PagesLikesView;
				OrderLikesButton.IsEnabled = false;
			};

			OrderLikesButton.Clicked += (sender, e) => {
				if (LikesSwitch.IsToggled)
				{
					OrderUserModel order = null;

					if (Segmented.SelectedValue == StringsRes.ProfileOptionTitle)
					{
						order = new OrderUserModel
						{
							FacebookId = Settings.IdUser,
							UserFacebookId = Settings.IdUser,
							Link = Settings.Link,
							FollowersCount = 0,
							FollowersOrder = (int)CountLikesSlider.Value
						};
					}
					else if (Segmented.SelectedValue == StringsRes.PagesOptionTitle)
					{
						var page = PostsPagesLikesView.Page;

						order = new OrderUserModel
						{
							FacebookId = page.id,
							UserFacebookId = Settings.IdUser,
							Link = page.link,
							FollowersCount = 0,
							FollowersOrder = (int)CountLikesSlider.Value
						};
					}

					int coins = int.Parse(App.CurrentUser.CustomProperties["Coins"].ToString()) - (int)CountLikesSlider.Value;

					if (coins > 0)
					{
						if (App.WSRest.CreateOrderUser(order) && App.WSRest.UpdateCoinsUser(coins))
						{
							DisplayAlert("", StringsRes.SuccessOrder, "OK");

							MessagingCenter.Send(this, "ReloadCoins");
						}
						else
						{
							DisplayAlert("", StringsRes.ErrorOrder, "OK");
						}
					}
					else
					{
						DisplayAlert("", StringsRes.ErrorCoinsOrder, "OK");
					}
				}
				else
				{
					PostModel post = null;

					if (Segmented.SelectedValue == StringsRes.ProfileOptionTitle)
					{
						post = ProfileLikesView.DataPost.data[ProfileLikesView.PostVisibleIndex];
					}
					else if (Segmented.SelectedValue == StringsRes.PagesOptionTitle)
					{
						post = PostsPagesLikesView.DataPost.data[PostsPagesLikesView.PostVisibleIndex];
					}

					var order = new OrderPostModel
					{
						FacebookId = post.id,
						UserFacebookId = Settings.IdUser,
						Link = post.actions[0].link,
						LikesCount = 0,
						LikesOrder = (int)CountLikesSlider.Value
					};

					int coins = int.Parse(App.CurrentUser.CustomProperties["Coins"].ToString()) - (int)CountLikesSlider.Value;

					if (coins > 0)
					{
						if (App.WSRest.CreateOrderPost(order) && App.WSRest.UpdateCoinsUser(coins))
						{
							DisplayAlert("", StringsRes.SuccessOrder, "OK");

							MessagingCenter.Send(this, "ReloadCoins");
						}
						else
						{
							DisplayAlert("", StringsRes.ErrorOrder, "OK");
						}
					}
					else
					{ 
						DisplayAlert("", StringsRes.ErrorCoinsOrder, "OK");
					}
				}
			};

			LikesSwitch.Toggled += (sender, e) => {
				var count = (int)CountLikesSlider.Value;
				CountLikesLabel.Text = "" + count;

				if (LikesSwitch.IsToggled)
				{					
					if (IsFeature)
						OrderLikesButton.Text = StringsRes.BuyLikes + " " + count.ToString() + " Followers";
					else
						OrderLikesButton.Text = StringsRes.OrderLikes + " " + count.ToString() + " Followers";
				}
				else
				{ 
					if (IsFeature)
						OrderLikesButton.Text = StringsRes.BuyLikes + " " + count.ToString() + " Likes";
					else
						OrderLikesButton.Text = StringsRes.OrderLikes + " " + count.ToString() + " Likes";					
				}
			};
		}
	}
}

