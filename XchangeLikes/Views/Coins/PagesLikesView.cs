﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace XchangeLikes
{
	public class PagesLikesView : ContentView
	{
		DataPagesModel DataPages;

		public ListView PagesListView;

		public PagesLikesView()
		{
			getPages();
			createElements();

			Content = PagesListView;
		}

		void getPages()
		{
			DataPages = App.WSRest.GetPages();
		}

		void createElements()
		{
			PagesListView = new ListView
			{
				RowHeight = 60,
				ItemTemplate = new DataTemplate(() =>
				{
					var menuLabel = new Label
					{
						FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
						VerticalTextAlignment = TextAlignment.Center,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					};

					var cel = new ImageCell();

					cel.SetBinding(ImageCell.TextProperty, "name");
					cel.SetBinding(ImageCell.ImageSourceProperty, "image");

					var cell = new ViewCell
					{						
						View = new StackLayout
						{
							VerticalOptions = LayoutOptions.CenterAndExpand,
							Children = {
								menuLabel
							}
						}
					};

					menuLabel.SetBinding(Label.TextProperty, "name");

					return cel;
				}),
			};

			if (DataPages != null)
			{
				if (DataPages.data != null)
					PagesListView.ItemsSource = DataPages.data;
			}
		}
	}
}

