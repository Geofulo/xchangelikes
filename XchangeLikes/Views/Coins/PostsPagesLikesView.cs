﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace XchangeLikes
{
	public class PostsPagesLikesView : ContentView
	{
		public PageModel Page;
		public DataPostModel DataPost;

		List<WebView> PostWebViews = new List<WebView>();

		ContentView PostView;

		Button PrevButton;
		Button NextButton;

		public int PostVisibleIndex = 0;

		public PostsPagesLikesView(PageModel Page)
		{
			this.Page = Page;

			setIndicator();

			VerticalOptions = LayoutOptions.FillAndExpand;

			Device.BeginInvokeOnMainThread(() =>
			{
				getPosts();
				createElements();
				createLayouts();
				setLayouts();
				setListeners();

				Content = new StackLayout
				{
					Orientation = StackOrientation.Horizontal,
					Children =
					{
						PrevButton,
						PostView,
						NextButton
					}
				};

				this.IsClippedToBounds = true;
			});

		}

		void setIndicator()
		{
			var indicator = new ActivityIndicator
			{
				IsRunning = true,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			Content = indicator;
		}

		void getPosts()
		{
			DataPost = App.WSRest.GetPostsFromPage(Page.id);
		}

		void createElements()
		{
			if (DataPost != null)
			{
				if (DataPost.data != null)
				{
					foreach (var p in DataPost.data)
					{
						PostWebViews.Add(new WebView
						{
							Source = p.actions[0].link,
							IsEnabled = false,
							TranslationY = -50,
							WidthRequest = App.ScreenWidth - 20,
							VerticalOptions = LayoutOptions.FillAndExpand,
						});

					}
				}
			}

			PrevButton = new Button
			{
				Text = "<",
			};
			NextButton = new Button
			{
				Text = ">",
			};
		}

		void createLayouts()
		{
			if (PostWebViews.Count > 0)
			{
				PostView = new ContentView
				{
					VerticalOptions = LayoutOptions.FillAndExpand
				};
			}
		}

		void setLayouts()
		{
			PostView.Content = PostWebViews[PostVisibleIndex];
		}

		void setListeners()
		{
			PrevButton.Clicked += (sender, e) =>
			{
				if (PostVisibleIndex > 0)
				{
					PostVisibleIndex--;
					PostView.Content = PostWebViews[PostVisibleIndex];
				}
			};
			NextButton.Clicked += (sender, e) =>
			{
				if (PostVisibleIndex + 1 < PostWebViews.Count)
				{
					PostVisibleIndex++;
					PostView.Content = PostWebViews[PostVisibleIndex];
				}
			};
		}

	}
}

