﻿using System;
using Xamarin.Forms;

namespace XchangeLikes
{
	public class FriendsLikesView : ContentView
	{
		DataFriendsModel DataFriends;

		public ListView FriendsListView;

		public FriendsLikesView()
		{
			getFriends();
			createElements();

			Content = FriendsListView;	
		}

		void getFriends()
		{
			DataFriends = App.WSRest.GetFriends();
		}

		void createElements()
		{			
			FriendsListView = new ListView
			{
				RowHeight = 60,
				ItemTemplate = new DataTemplate(() =>
				{
					var menuLabel = new Label
					{
						FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
						VerticalTextAlignment = TextAlignment.Center,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					};

					var cel = new ImageCell();

					cel.SetBinding(ImageCell.TextProperty, "name");
					cel.SetBinding(ImageCell.ImageSourceProperty, "image");

					var cell = new ViewCell
					{
						View = new StackLayout
						{
							VerticalOptions = LayoutOptions.CenterAndExpand,
							Children = {
								menuLabel
							}
						}
					};

					menuLabel.SetBinding(Label.TextProperty, "name");

					return cel;
				}),
			};

			if (DataFriends != null)
			{
				if (DataFriends.data != null)
					FriendsListView.ItemsSource = DataFriends.data;
			}
		}
	}
}

