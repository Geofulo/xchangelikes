﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using XchangeLikes.Helpers;
using XchangeLikes.Resources;

namespace XchangeLikes
{
	public class ProfileLikesView : ContentView
	{
		public DataPostModel DataPost;

		List<WebViewCustom> PostWebViews = new List<WebViewCustom>();

		ContentViewCustom PostView;

		Button PrevButton;
		Button NextButton;

		public int PostVisibleIndex = 0;

		public ProfileLikesView()
		{
			setIndicator();

			VerticalOptions = LayoutOptions.FillAndExpand;

			Device.BeginInvokeOnMainThread(() =>
			{
				getPosts();
				createElements();
				createLayouts();
				setLayouts();
				setListeners();

				Content = new StackLayout
				{
					//TranslationY = -50,
					BackgroundColor = Color.Transparent,
					Orientation = StackOrientation.Horizontal,
					Children =
						{
							PrevButton,
							PostView,
							NextButton
						}
				};
				//PostView.HeightRequest += 100;
				this.IsClippedToBounds = true;
				//this.TranslationY = -50;
			});
		}

		void setIndicator()
		{
			var indicator = new ActivityIndicator
			{
				IsRunning = true,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			Content = indicator;
		}

		void getPosts()
		{
			DataPost = App.WSRest.GetPosts();
		}

		void createElements()
		{
			if (DataPost != null)
			{
				if (DataPost.data != null)
				{
					foreach (var p in DataPost.data)
					{

						//wv.HeightRequest += 100;

						//wv.LayoutTo(new Rectangle(0, 0, wv.Width, wv.Height));

						PostWebViews.Add(new WebViewCustom
						{
							Source = p.actions[0].link,
							IsEnabled = false,
							WidthRequest = 400,
							HeightRequest = 400,
							//HorizontalOptions = LayoutOptions.CenterAndExpand,
							//VerticalOptions = LayoutOptions.FillAndExpand,
						});

						//wv.Margin = new Thickness(0, -50, 0, 0);
					}
				}
			}

			PrevButton = new Button
			{
				Text = "<",
				HorizontalOptions = LayoutOptions.Start,
			};
			NextButton = new Button
			{
				Text = ">",
				HorizontalOptions = LayoutOptions.End,
			};
		}

		void createLayouts()
		{
			PostView = new ContentViewCustom { 				
				TranslationY = -50,
				//IsClippedToBounds = true,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
		}

		void setLayouts()
		{
			if (PostWebViews.Count > 0)
				PostView.Content = PostWebViews[PostVisibleIndex];			
		}

		void setListeners()
		{
			PrevButton.Clicked += (sender, e) => {
				if (PostVisibleIndex > 0 && PostWebViews.Count > 0)
				{
					PostVisibleIndex--;
					PostView.Content = PostWebViews[PostVisibleIndex];
				}
			};
			NextButton.Clicked += (sender, e) => {
				if (PostVisibleIndex + 1 < PostWebViews.Count && PostWebViews.Count > 0)
				{
					PostVisibleIndex++;
					PostView.Content = PostWebViews[PostVisibleIndex];
				}
			};
		}
	}
}

