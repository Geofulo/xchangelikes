﻿using System;
using Xamarin.Forms;
using XchangeLikes.Resources;

namespace XchangeLikes
{
	public class EarnCoinsPage : ContentPage
	{
		SegmentedControl Segmented;

		SegmentedControlOption LikesOption;
		SegmentedControlOption FollowersOption;

		LikesCoinsView LikesView;
		FollowersCoinsView FollowersView;
		ContentView EarnCoinsView;

		StackLayout Stack;

		public EarnCoinsPage()
		{
			Title = StringsRes.EarnCoinsTitle;

			createElements();
			createLayouts();
			setLayouts();
			setListeners();

			//Padding = new Thickness(0, 15, 0, 0);
			Content = Stack;
		}

		void createElements()
		{
			Segmented = new SegmentedControl();

			LikesOption = new SegmentedControlOption
			{
				Text = StringsRes.LikesOptionTitle
			};

			FollowersOption = new SegmentedControlOption
			{
				Text = StringsRes.FollowersOptionTitle
			};

			Segmented.Children.Add(LikesOption);
			Segmented.Children.Add(FollowersOption);
		}

		void createLayouts()
		{
			LikesView = new LikesCoinsView();
			FollowersView = new FollowersCoinsView();

			EarnCoinsView = new ContentView
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout
			{
				Spacing = 10,
				Padding = new Thickness(10, 15, 10, 0),
			};
		}

		void setLayouts()
		{
			EarnCoinsView.Content = LikesView;

			Stack.Children.Add(Segmented);
			Stack.Children.Add(EarnCoinsView);
		}

		void setListeners()
		{
			Segmented.ValueChanged += (sender, e) =>
			{
				System.Diagnostics.Debug.WriteLine(Segmented.SelectedValue);

				if (Segmented.SelectedValue == StringsRes.LikesOptionTitle)
				{
					EarnCoinsView.Content = LikesView;
				}
				if (Segmented.SelectedValue == StringsRes.FollowersOptionTitle)
				{
					EarnCoinsView.Content = FollowersView;
				}
			};
		}
	}
}

