﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using XchangeLikes.Resources;

namespace XchangeLikes
{
	public class LikesCoinsView : ContentView
	{
		List<OrderPostModel> Orders = new List<OrderPostModel>();

		ContentViewCustom MainView;

		Button SkipButton;
		Button LikeButton;

		int OrderVisibleIndex = 0;

		public LikesCoinsView()
		{
			getOrders();
			createElements();
			setListeners();

			Content = new StackLayout
			{
				Spacing = 15,
				Children = {
					MainView,
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						IsClippedToBounds = true,
						VerticalOptions = LayoutOptions.End,
						Children = {
							SkipButton,
							LikeButton
						}
					}
				}
			};
			this.IsClippedToBounds = true;
		}

		void getOrders()
		{
			Orders = App.WSRest.GetLikeOrders();
		}

		void createElements()
		{
			MainView = new ContentViewCustom { 
				TranslationY = -50,
				HeightRequest = 500,
				//VerticalOptions = LayoutOptions.FillAndExpand
			};

			System.Diagnostics.Debug.WriteLine(Helpers.Settings.AccessToken);

			var http = new HtmlWebViewSource
			{
				Html = "" + 
					//"<iframe src=\"https://www.facebook.com/plugins/post.php?" +
					//"href=https://www.facebook.com/20531316728/posts/10154009990506729/" +
					//"href=https%3A%2F%2Fwww.facebook.com%2F20531316728%2Fposts%2F10154009990506729%2F" +
					"<iframe scr=\"https://www.facebook.com/1168911186504909/posts/1128543153875046\"" +
					//"width=300" +
					//"&show_text=true" +
					//"&appId=246878555693567" +
					//"&height=600' " +
					"width=\"300\" height=\"600\" " +
					"style=\"border:none;overflow:hidden\" " +
					"scrolling=\"no\" " +
					"frameborder=\"0\" " +
					"allowTransparency=\"true\">" +
					"</iframe>",
			};

			var httpSource = new HtmlWebViewSource
			{
				Html = "<html>" +					
					"<body>" +
					"<div id=\"fb-root\"></div>\n<script>(function(d, s, id) {\n  var js, fjs = d.getElementsByTagName(s)[0];\n  if (d.getElementById(id)) return;\n  js = d.createElement(s); js.id = id;\n  js.src = \"//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.6&appId=408832435984255\";\n  fjs.parentNode.insertBefore(js, fjs);\n}(document, 'script', 'facebook-jssdk'));</script>" + 
					//"<div class=\"fb-post\" data-href=\"https://www.facebook.com/20531316728/posts/10154009990506729/\" data-width=\"500\" data-show-text=\"true\">" +
					"<div class=\"fb-post\" data-href=\"https://www.facebook.com/20531316728/posts/10154009990506729/\" data-width=\"500\"></div>" +
					//"</div>" +
					"</body></html>",
			};

			if (Orders.Count > 0)
			{
				var wv = new WebViewCustom
				{
					//Source = "https://www.facebook.com/photo.php?fbid=1155037014558993&set=a.599918593404174.1073741826.100001581162412&type=3",
					Source = Orders[OrderVisibleIndex].Link,
					//Source = http,
					IsEnabled = false,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
				};

				System.Diagnostics.Debug.WriteLine(wv.AnchorY + " " + wv.Bounds.Height + " " + wv.Height + " " + wv.HeightRequest);

				MainView.Content = wv;
			}
			else
			{ 
				MainView.Content = new Label
				{
					Text = StringsRes.NoMoreOrdersLabel,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand
				};
			}

			SkipButton = new Button
			{
				Text = StringsRes.SkipButton,
				TextColor = Color.Red,
				BorderColor = Color.Red,
				BorderWidth = 1,
				Margin = new Thickness(5, 0),
				HorizontalOptions = LayoutOptions.StartAndExpand,
			};

			LikeButton = new Button
			{
				Text = "Like (+0.5 coins)",
				BackgroundColor = Color.Silver,
				HorizontalOptions = LayoutOptions.EndAndExpand,
			};
		}

		void setListeners()
		{
			SkipButton.Clicked += (sender, e) => skipPost();

			LikeButton.Clicked += (sender, e) => {
				if (App.WSRest.CreateLike(Orders[OrderVisibleIndex].FacebookId))
				{
					skipPost();
				}
				else
				{
					Application.Current.MainPage.DisplayAlert("", StringsRes.ErrorLike, "OK");
				}
			};
		}

		void skipPost()
		{
			OrderVisibleIndex++;

			if (OrderVisibleIndex < Orders.Count)
			{
				var wv = new WebViewCustom
				{
					Source = Orders[OrderVisibleIndex].Link,
					IsEnabled = false,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
				};
				System.Diagnostics.Debug.WriteLine(wv.AnchorY + " " + wv.Bounds.Height + " " + wv.Height + " " + wv.HeightRequest);

				MainView.Content = wv;

				System.Diagnostics.Debug.WriteLine(MainView.AnchorY + " " + MainView.Bounds.Height + " " + MainView.Height + " " + MainView.HeightRequest);
			}
			else
			{
				OrderVisibleIndex = 0;

				MainView.Content = new Label
				{
					Text = StringsRes.NoMoreOrdersLabel,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand
				};
			}
		}
	}
}

