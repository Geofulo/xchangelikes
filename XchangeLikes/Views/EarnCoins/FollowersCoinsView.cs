﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using XchangeLikes.Resources;

namespace XchangeLikes
{
	public class FollowersCoinsView : ContentView
	{
		List<OrderUserModel> Orders = new List<OrderUserModel>();

		ContentView MainView;

		Button SkipButton;
		Button FollowButton;

		int OrderVisibleIndex = 0;

		public FollowersCoinsView()
		{
			getOrders();
			createElements();
			setListeners();

			Content = new StackLayout
			{
				Spacing = 15,
				Children = {
					MainView,
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						IsClippedToBounds = true,
						VerticalOptions = LayoutOptions.End,
						Children = {
							SkipButton,
							FollowButton
						}
					}
				}
			};
			this.IsClippedToBounds = true;
		}

		void getOrders()
		{
			Orders = App.WSRest.GetFollowOrders();
		}

		void createElements()
		{
			MainView = new ContentView
			{
				TranslationY = -100,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			if (Orders.Count > 0)
			{
				var wv = new WebViewCustom
				{
					Source = Orders[OrderVisibleIndex].Link,
					IsEnabled = false,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
				};

				System.Diagnostics.Debug.WriteLine(wv.AnchorY + " " + wv.Bounds.Height + " " + wv.Height + " " + wv.HeightRequest);

				MainView.Content = wv;
			}
			else
			{ 
				MainView.Content = new Label
				{
					Text = StringsRes.NoMoreOrdersLabel,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand
				};
			}

			SkipButton = new Button
			{
				Text = StringsRes.SkipButton,
				TextColor = Color.Red,
				BorderColor = Color.Red,
				BorderWidth = 1,
				Margin = new Thickness(5, 0),
				HorizontalOptions = LayoutOptions.StartAndExpand,
			};

			FollowButton = new Button
			{
				Text = "Follow (+0.5 coins)",
				BackgroundColor = Color.Silver,
				HorizontalOptions = LayoutOptions.EndAndExpand,
			};
		}

		void setListeners()
		{
			SkipButton.Clicked += (sender, e) => skipPost();

			FollowButton.Clicked += (sender, e) =>
			{
				
				if (App.WSRest.CreateFollower(Orders[OrderVisibleIndex].FacebookId))
				{
					skipPost();
				}
				else
				{
					Application.Current.MainPage.DisplayAlert("", StringsRes.ErrorLike, "OK");
				}

			};
		}

		void skipPost()
		{
			OrderVisibleIndex++;

			if (OrderVisibleIndex < Orders.Count)
			{
				var wv = new WebViewCustom
				{
					Source = Orders[OrderVisibleIndex].Link,
					IsEnabled = false,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
				};
				System.Diagnostics.Debug.WriteLine(wv.AnchorY + " " + wv.Bounds.Height + " " + wv.Height + " " + wv.HeightRequest);

				MainView.Content = wv;

				System.Diagnostics.Debug.WriteLine(MainView.AnchorY + " " + MainView.Bounds.Height + " " + MainView.Height + " " + MainView.HeightRequest);
			}
			else
			{
				OrderVisibleIndex = 0;

				MainView.Content = new Label
				{
					Text = StringsRes.NoMoreOrdersLabel,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand
				};
			}
		}
	}
}

