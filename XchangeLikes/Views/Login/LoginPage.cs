﻿using System;
using Xamarin.Forms;

namespace XchangeLikes
{
	public class LoginPage : ContentPage
	{
		Button LoginFacebookButton;

		public LoginPage()
		{
			Title = "Login";

			createElements();
			setListeners();

			Content = new StackLayout
			{
				Children = {
					LoginFacebookButton
				}
			};
		}

		void createElements()
		{
			LoginFacebookButton = new Button
			{
				Text = "Login with Facebook",
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
		}

		void setListeners()
		{
			LoginFacebookButton.Clicked += (sender, e) => {
				DependencyService.Get<IFacebook>().Login();
			};
		}
}
}

