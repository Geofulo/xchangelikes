﻿using System;
using Xamarin.Forms;
using XchangeLikes.Resources;

namespace XchangeLikes
{
	public class MorePage : ContentPage
	{
		TableView MoreTableView;

		TextCell FreeCoinsCell;
		TextCell WatchVideoCell;
		TextCell InviteFriendCell;
		TextCell RateAppCell;
		TextCell SupportCell;
		TextCell LogoutCell;

		public MorePage()
		{
			Title = StringsRes.MoreTitle;

			createElements();
			setListeners();

			Content = MoreTableView;
		}

		void createElements()
		{
			FreeCoinsCell = new TextCell
			{				
				Text = StringsRes.FreeCoinsText,
			};
			WatchVideoCell = new TextCell
			{
				Text = StringsRes.WatchVideoText,
			};
			InviteFriendCell = new TextCell
			{
				Text = StringsRes.InviteFriendText,
			};
			RateAppCell = new TextCell
			{
				Text = StringsRes.RateAppText,
			};
			SupportCell = new TextCell
			{
				Text = StringsRes.SupportText,
			};
			LogoutCell = new TextCell
			{
				Text = StringsRes.LogoutText,
			};

			MoreTableView = new TableView
			{
				Intent = TableIntent.Settings,
				Root = new TableRoot
				{
					new TableSection  {
						FreeCoinsCell,
						WatchVideoCell,
						InviteFriendCell
					},
					new TableSection {
						RateAppCell,
						SupportCell
					},
					new TableSection {
						LogoutCell
					}
				}
			};
		}

		void setListeners()
		{
			FreeCoinsCell.Tapped += (sender, e) => { 
			
			};

			WatchVideoCell.Tapped += (sender, e) =>
			{

			};

			InviteFriendCell.Tapped += (sender, e) =>
			{

			};

			RateAppCell.Tapped += (sender, e) =>
			{

			};

			SupportCell.Tapped += (sender, e) =>
			{

			};

			LogoutCell.Tapped += (sender, e) =>
			{

			};
		}
	}
}

