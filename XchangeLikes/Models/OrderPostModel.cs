﻿using System;
using Telerik.Everlive.Sdk.Core.Model.Base;
using Telerik.Everlive.Sdk.Core.Serialization;

namespace XchangeLikes
{
	[ServerType("OrderPost")]
	public class OrderPostModel : DataItem
	{
		public string FacebookId { get; set; }
		public string UserFacebookId { get; set; }
		public string Link { get; set; }
		public int LikesOrder { get; set; }
		public int LikesCount { get; set; }
	}
}

