﻿using System;
using Telerik.Everlive.Sdk.Core.Model.Base;
using Telerik.Everlive.Sdk.Core.Serialization;

namespace XchangeLikes
{
	[ServerType("OrderUser")]
	public class OrderUserModel : DataItem
	{
		public string FacebookId { get; set; }
		public string UserFacebookId { get; set; }
		public string Link { get; set; }
		public int FollowersOrder { get; set; }
		public int FollowersCount { get; set; }
	}
}

