﻿using System;
using Telerik.Everlive.Sdk.Core.Model.System;
using Xamarin.Forms;
using XchangeLikes.Helpers;

namespace XchangeLikes
{
	public class App : Application
	{
		public static int ScreenWidth;
		public static int ScreenHeight;
		public static RestService WSRest;
		public static User CurrentUser;

		public App()
		{
			WSRest = new RestService();

			if (string.IsNullOrEmpty(Settings.AccessToken) || string.IsNullOrEmpty(Settings.TelerikIdUser))
			{
				MainPage = new LoginPage();
			}
			else 
			{
				if (WSRest.GetMyId()) {
					//CurrentUser = WSRest.GetUser(Guid.Parse(Settings.TelerikIdUser));
					CurrentUser = WSRest.GetUser(new Guid(Settings.TelerikIdUser));
					//MainPage = new MainPage();
					MainPage = new NavigationPage(new MainPage());
				} else
					MainPage = new LoginPage();
			}
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}

