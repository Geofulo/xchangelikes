﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Telerik.Everlive.Sdk.Core;
using Telerik.Everlive.Sdk.Core.Model.System;
using Telerik.Everlive.Sdk.Core.Query.Definition.Updating;
using Xamarin.Forms;
using XchangeLikes.Helpers;

namespace XchangeLikes
{
	public class RestService
	{
		HttpClient Client;
		static string _uriBase = "https://www.facebook.com/";
		static string _uriBaseGraph = "https://graph.facebook.com/";
		static string _typeContent = "application/json";

		EverliveApp EveApp;

		public RestService()
		{
			Client = new HttpClient();

			EverliveAppSettings appSettings = new EverliveAppSettings
			{
				AppId = "gnail96sdd16s09p",
				//TestMode = true,
				UseHttps = true
			};
			EveApp = new EverliveApp(appSettings);
		}

		public bool GetMyId()
		{ 
			System.Diagnostics.Debug.WriteLine("REST GetMyId...");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(_uriBaseGraph + "v2.6/me" + "?fields=link,name" + "&access_token=" + Settings.AccessToken);

					var res = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Success: " + response.StatusCode + " " + res);

						var jsonRes = JsonConvert.DeserializeObject<IDictionary<string, string>>(res);

						System.Diagnostics.Debug.WriteLine(jsonRes["id"]);
						System.Diagnostics.Debug.WriteLine(jsonRes["name"]);
						System.Diagnostics.Debug.WriteLine(jsonRes["link"]);


						Settings.IdUser = jsonRes["id"];
						Settings.Name = jsonRes["name"];
						Settings.Link = jsonRes["link"];

						tcs.SetResult(true);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + res);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public DataPostModel GetPosts()
		{
			System.Diagnostics.Debug.WriteLine("REST GetPosts...");

			var tcs = new TaskCompletionSource<DataPostModel>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(_uriBaseGraph + "v2.6/me/posts" + "?fields=id,message,story,created_time,actions" + "&limit=50" + "&access_token=" + Settings.AccessToken);

					var res = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Success: " + response.StatusCode + " " + res);

						var jsonRes = JsonConvert.DeserializeObject<DataPostModel>(res);

						if (jsonRes != null)
							tcs.SetResult(jsonRes);
						else
							tcs.SetResult(null);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + res);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public DataPostModel GetPosts(string urlPag)
		{
			System.Diagnostics.Debug.WriteLine("REST GetPosts(string urlPag)...");

			var tcs = new TaskCompletionSource<DataPostModel>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(urlPag + "?access_token=" + Settings.AccessToken);

					var res = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Success: " + response.StatusCode + " " + res);

						var jsonRes = JsonConvert.DeserializeObject<DataPostModel>(res);

						if (jsonRes != null)
							tcs.SetResult(jsonRes);
						else
							tcs.SetResult(null);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + res);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public DataPagesModel GetPages()
		{
			System.Diagnostics.Debug.WriteLine("REST GetPages...");

			var tcs = new TaskCompletionSource<DataPagesModel>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(_uriBaseGraph + "v2.6/me/accounts" + "?fields=picture,access_token,name,id,link" + "&access_token=" + Settings.AccessToken);

					var res = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Success: " + response.StatusCode + " " + res);

						var jsonRes = JsonConvert.DeserializeObject<DataPagesModel>(res);

						if (jsonRes != null)
							tcs.SetResult(jsonRes);
						else
							tcs.SetResult(null);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + res);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public DataPostModel GetPostsFromPage(string idPage)
		{
			System.Diagnostics.Debug.WriteLine("REST GetPostsFromPage...");

			var tcs = new TaskCompletionSource<DataPostModel>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(_uriBaseGraph + "v2.6/" + idPage + "/posts" + "?fields=id,message,story,created_time,actions" + "&limit=50" + "&access_token=" + Settings.AccessToken);

					var res = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Success: " + response.StatusCode + " " + res);

						var jsonRes = JsonConvert.DeserializeObject<DataPostModel>(res);

						if (jsonRes != null)
							tcs.SetResult(jsonRes);
						else
							tcs.SetResult(null);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + res);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public DataFriendsModel GetFriends()
		{
			System.Diagnostics.Debug.WriteLine("REST GetFriends...");

			var tcs = new TaskCompletionSource<DataFriendsModel>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(_uriBaseGraph + "v2.6/me/friends" + "?fields=picture,name,id" + "&limit=50" + "&access_token=" + Settings.AccessToken);

					var res = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Success: " + response.StatusCode + " " + res);

						var jsonRes = JsonConvert.DeserializeObject<DataFriendsModel>(res);

						if (jsonRes != null)
							tcs.SetResult(jsonRes);
						else
							tcs.SetResult(null);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + res);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool CreateLike(string id)
		{
			System.Diagnostics.Debug.WriteLine("REST CreateLike...");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.PostAsync(_uriBaseGraph + "v2.6/" + id + "/likes" + "?access_token=" + Settings.AccessToken, null);

					var res = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Success: " + response.StatusCode + " " + res);

						tcs.SetResult(true);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + res);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool LoginWithFacebook(string token)
		{
			System.Diagnostics.Debug.WriteLine("Telerik LoginWithFacebook...");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var result = await EveApp.WorkWith().Authentication().LoginWithFacebook(token).ExecuteAsync();

					System.Diagnostics.Debug.WriteLine("Custom Properties: " + result.CustomProperties.Count);

					Settings.TelerikIdUser = result.PrincipalId.ToString();
					App.CurrentUser = GetUser(result.PrincipalId);

					System.Diagnostics.Debug.WriteLine("Login correcto, usuario " + result.PrincipalId);

					tcs.SetResult(true);
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine(e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public User GetUser(Guid id)
		{
			System.Diagnostics.Debug.WriteLine("Telerik GetUser...");

			var tcs = new TaskCompletionSource<User>();

			new Task(async () =>
			{	
				try
				{
					var user = await EveApp.WorkWith().Users().GetById(id).ExecuteAsync();

					if (!user.CustomProperties.ContainsKey("Coins"))
					{
						System.Diagnostics.Debug.WriteLine("No User Coins");

						UpdateObject updateObject = new UpdateObject();
						updateObject.UpdatedFields.Add(new UpdatedField()
						{
							FieldName = "Coins",
							Value = 40
						});

						var res = await EveApp.WorkWith().Users().Update(updateObject).Where(u => u.Id == user.Id).ExecuteAsync();

						user = await EveApp.WorkWith().Users().GetById(id).ExecuteAsync();
					}

					tcs.SetResult(user);
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine(e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool UpdateCoinsUser(int coins)
		{
			System.Diagnostics.Debug.WriteLine("Telerik UpdateCoinsUser...");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					UpdateObject updateObject = new UpdateObject();
					updateObject.UpdatedFields.Add(new UpdatedField()
					{
						FieldName = "Coins",
						Value = coins
					});

					var res = await EveApp.WorkWith().Users().Update(updateObject).Where(u => u.Id == App.CurrentUser.Id).ExecuteAsync();

					App.CurrentUser = GetUser(App.CurrentUser.Id);

					System.Diagnostics.Debug.WriteLine("Actualización del usuario " + App.CurrentUser.Id);

					tcs.SetResult(true);
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine(e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool CreateOrderPost(OrderPostModel orderPost)
		{
			System.Diagnostics.Debug.WriteLine("Telerik CreateOrderPost...");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var result = await EveApp.WorkWith().Data<OrderPostModel>().Create(orderPost).ExecuteAsync();

					System.Diagnostics.Debug.WriteLine("Pedido " + result.Id + " creado");

					tcs.SetResult(true);
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine(e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public List<OrderPostModel> GetLikeOrders()
		{
			System.Diagnostics.Debug.WriteLine("Telerik GetLikeOrders...");

			var tcs = new TaskCompletionSource<List<OrderPostModel>>();

			var res = new List<OrderPostModel>();

			new Task(async () =>
			{
				try
				{					
					var result = await EveApp.WorkWith().Data<OrderPostModel>().Get().Where(q => q.UserFacebookId != Settings.IdUser && q.LikesCount < q.LikesOrder).ExecuteAsync();

					if (result != null)
					{
						foreach (var r in result)
						{
							res.Add(r);
						}
					}

					System.Diagnostics.Debug.WriteLine(res.Count + " pedidos");
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine(e.Message);
				}
				tcs.SetResult(res);
			}).Start();

			return tcs.Task.Result;
		}

		public bool CreateOrderUser(OrderUserModel order)
		{
			System.Diagnostics.Debug.WriteLine("Telerik CreateOrderUser...");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var result = await EveApp.WorkWith().Data<OrderUserModel>().Create(order).ExecuteAsync();

					System.Diagnostics.Debug.WriteLine("Pedido " + result.Id + " creado");

					tcs.SetResult(true);
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine(e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public List<OrderUserModel> GetFollowOrders()
		{
			System.Diagnostics.Debug.WriteLine("Telerik GetFollowOrders...");

			var tcs = new TaskCompletionSource<List<OrderUserModel>>();

			var res = new List<OrderUserModel>();

			new Task(async () =>
			{
				try
				{
					var result = await EveApp.WorkWith().Data<OrderUserModel>().Get().Where(q => q.UserFacebookId != Settings.IdUser && q.FollowersCount < q.FollowersOrder).ExecuteAsync();

					if (result != null)
					{
						foreach (var r in result)
						{
							res.Add(r);
						}
					}

					System.Diagnostics.Debug.WriteLine(res.Count + " pedidos");
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine(e.Message);
				}
				tcs.SetResult(res);
			}).Start();

			return tcs.Task.Result;
		}

		public bool CreateFollower(string id)
		{
			System.Diagnostics.Debug.WriteLine("REST CreateFollower...");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.PostAsync(_uriBaseGraph + "v2.6/me/friends/" + id + "?access_token=" + Settings.AccessToken, null);

					var res = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Success: " + response.StatusCode + " " + res);

						tcs.SetResult(true);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + res);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}
	}



	public class DataPostModel
	{
		public PostModel[] data { get; set; }
		public PagingModel paging { get; set; }
	}

	public class PostModel
	{
		public string message { get; set; }
		public string story { get; set; }
		public string created_time { get; set; }
		public string id { get; set; }
		public ActionModel[] actions { get; set; }
	}

	public class ActionModel
	{
		public string name { get; set; }
		public string link { get; set; }
	}

	public class PagingModel
	{
		public string previous { get; set; }
		public string next { get; set; }
	}

	public class DataPagesModel
	{
		public PageModel[] data { get; set; }
	}

	public class PageModel
	{
		public string access_token { get; set; }
		public string name { get; set; }
		public string id { get; set; }
		public string link { get; set; }
		public PictureModel picture { get; set; }

		public ImageSource image
		{
			get { return ImageSource.FromUri(new Uri(picture.data.url)); }
		}
	}

	public class PictureModel
	{
		public DataPictureModel data { get; set; }
	}

	public class DataPictureModel
	{
		public string url { get; set; }
	}

	public class DataFriendsModel
	{
		public FriendModel[] data { get; set; }
	}

	public class FriendModel
	{		
		public string name { get; set; }
		public string id { get; set; }
		public PictureModel picture { get; set; }

		public ImageSource image
		{
			get { return ImageSource.FromUri(new Uri(picture.data.url)); }
		}
	}


}

